import numpy as np
import pandas as pd
import sys
import os
from collections import defaultdict
from scipy.stats import norm
from scipy.optimize import minimize,minimize_scalar

#R
import rpy2
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
import rpy2.robjects as robjects
from rpy2.robjects.functions import SignatureTranslatedFunction



def call(infolder, plotLikelihood=True, plotTargets=True, prob_thresh=0.5, prior=0.1):

    if plotLikelihood or plotTargets:
        import matplotlib.pyplot as plt

    pandas2ri.activate()
    
    ### DEL GAIN FIT
    robjects.r('''
    norm_gain_del_loglikelihood<-function(t,x){
        s<-t[1]
        p<-t[2]
        q<-t[3]
        TMP <- (1-p-q)*dnorm(x,1,s) + p*dnorm(x,0.5,s) + q*dnorm(x,1.5,s) 
        TMP[which(TMP<1e-323)] <- 1e-323
        -sum(log(TMP))
    }''')
    optimizeDelGainNorm = robjects.r('''
    optimizeDelGainNorm <- function(xvec){
        init <- c( 0, sd(xvec), 0.1, 0.1)
        optim(init, norm_gain_del_loglikelihood, x=xvec, lower=c(0.025,0,0), upper=c(1e3,0.5,0.5))
    }''')





    #TODO report TARGETS that could not be evaluated
    FILES = [_ for _ in os.listdir(infolder) if _.endswith('.tsv') and not _.endswith('_all.tsv') and not _.endswith('_se.tsv')]
    #check if required files exist
    FILES = [_ for _ in FILES if os.path.exists(os.path.join(infolder, _[:-4]+'_all.tsv')) and os.path.exists(os.path.join(infolder, _[:-4]+'_fall_se.tsv'))]


    SAMPLES = []
    CALLS = defaultdict(dict)
    #NOT_EVALUABLE = defaultdict(defaultdict(list))

    gain_prior_prob = prior
    del_prior_prob = prior
    cn2_prior_prob = 1- gain_prior_prob - del_prior_prob

    for FILE in FILES:


        SAMPLE = '_'.join(FILE.split('_')[2:])[:-4] #TODO: e.g. target ATM_SNP 
        # XXX Achim's APPROACH: get data point with min(all_se/fall)
        data_all = pd.read_csv(os.path.join(infolder, FILE[:-4]+'_all.tsv'), sep='\t')
        data_se_all = pd.read_csv(os.path.join(infolder, FILE[:-4]+'_fall_se.tsv'), sep='\t')
        SAMPLES = data_all.columns.values[4:]
        GENE, TARGET_IND, CHROM = data_all.iloc[0,0], data_all.iloc[0,1], data_all.iloc[0,2]
        #delete negative values: avoid CNV calling at positions with negative fall
        POS_INDS = data_all[(data_all[SAMPLES]>0).all(axis=1)].index
        data_all = data_all.iloc[POS_INDS]
        data_se_all = data_se_all.iloc[POS_INDS]
        data_all.index = range(len(data_all))
        data_se_all.index = data_all.index 
        MIN_POINTS = np.ones(len(SAMPLES))
        #XXX TODO subtarget-wise call???
        for i in range(len(SAMPLES)):
            #TODO catch if all POS_INDS<0
            MIN_POINTS[i] = np.argmin(data_se_all[SAMPLES[i]]/data_all[SAMPLES[i]] )
        MIN_IND = int(round(np.mean(MIN_POINTS),0))
        #del data_all
        del data_se_all
        data = pd.read_csv(os.path.join(infolder, FILE), sep='\t')
        data = data.iloc[POS_INDS]
        SAMPLES = data.columns.values[4:]
        
        L =len(data)
        data = data.iloc[MIN_IND,:]
        data_all = data_all.iloc[MIN_IND,4::]
        #get exponents
        #XXX
        data.iloc[4:] = np.exp(np.array(data.iloc[4:], dtype=float))
        plotX = np.arange(0,2,0.0001)
        
        
        D = np.array(data.iloc[4:], dtype=float)

        PARAM = optimizeDelGainNorm(D[np.where(D<=2)]) #s,p,q
        if plotLikelihood:
            fig, ax = plt.subplots()
            p = PARAM[0][1]
            q = PARAM[0][2]
            plt.title('{}\np={}, q={}\n(1-p-q)*norm.pdf(x,1,{}) + p*norm.pdf(x,0.5,{}) + q*norm.pdf(x,1.5,{})'.format(PARAM[1], p, q, PARAM[0][0], PARAM[0][0], PARAM[0][0] ), fontsize=4)
            plt.plot(plotX, (1-p-q)*norm.pdf(plotX,1,PARAM[0][0]), 'b')
            plt.plot(plotX,p*norm.pdf(plotX,0.5,PARAM[0][0]), 'b', zorder=13) #dnorm(xvec,1,s)
            plt.plot(plotX,q*norm.pdf(plotX,1.5,PARAM[0][0]), 'b', zorder=13)
            
            locs = ax.yaxis.get_ticklocs()
            MAX_Y = max((1-p-q)*norm.pdf(plotX,1,PARAM[0][0]))
            plt.boxplot(D, 1, 'k.',vert=0, positions=np.array([MAX_Y+0.5]), labels = [''])
            plt.ylim((0, np.ceil(MAX_Y+1) ) )
            plt.yticks(locs, [str(_) for _ in locs])
            
        #p>0 or q>0 
        if (PARAM[0][1]>0) or (PARAM[0][2] >0):
        
            param = (PARAM[0][0], PARAM[0][1], PARAM[0][2])
            #p>0 DEL?
            if PARAM[0][1]>0:
                #deletion?
                i = list(SAMPLES).index(SAMPLE)
                
                pdx = norm.pdf(D[i], loc=0.5, scale=param[0])
                pnx = norm.pdf(D[i], loc=1, scale=param[0])
                pax = norm.pdf(D[i], loc=1.5, scale=param[0])
                
                #TODO user-defined prior probs
                s = pdx*del_prior_prob + pnx*cn2_prior_prob + pax*gain_prior_prob
                #DEBUG
                #print(GENE, TARGET_IND, SAMPLES[0])
                #print('=>', pdx*1/3, pnx*1/3, pax*1/3, pdx*1/3 * 1/s )

                
                PDEL = pdx * del_prior_prob * 1/s
                if PDEL > prob_thresh:
                    I=0
                    if CHROM in CALLS[SAMPLES[i]]: 
                        CALLS[SAMPLES[i]][CHROM].append((GENE, TARGET_IND, 1, D[i], PDEL, param[0], np.mean(data_all)))
                    else:
                        CALLS[SAMPLES[i]][CHROM]= [(GENE, TARGET_IND, 1, D[i], PDEL, param[0], np.mean(data_all))]
                
            #q>0 GAIN? 
            if PARAM[0][2]>0: 
                i = list(SAMPLES).index(SAMPLE)
                pdx = norm.pdf(D[i], loc=0.5, scale=param[0])
                pnx = norm.pdf(D[i], loc=1, scale=param[0])
                pax = norm.pdf(D[i], loc=1.5, scale=param[0])
                #s = pdx*1/3 + pnx*1/3 + pax*1/3
                s = pdx*del_prior_prob + pnx*cn2_prior_prob + pax*gain_prior_prob
                PGAIN = pax* gain_prior_prob * 1/s
                if PGAIN > prob_thresh:
                    I=0
                    if CHROM in CALLS[SAMPLES[i]]: 
                        CALLS[SAMPLES[i]][CHROM].append((GENE, TARGET_IND, 3, D[i], PGAIN, param[0], np.mean(data_all)))
                    else:
                        CALLS[SAMPLES[i]][CHROM]= [(GENE, TARGET_IND, 3, D[i], PGAIN, param[0], np.mean(data_all))]

        if plotLikelihood:
            plt.savefig(FILE.split('.')[0] + '.opacnat.pdf')
            plt.close()

        if plotTargets:
            plot_target(os.path.join(infolder, FILE), MIN_IND, SAMPLE)

    
    for SAMPLE in CALLS.keys():
        print('#', SAMPLE)
        for CHROM in sorted(CALLS[SAMPLE]):
            for entry in sorted(CALLS[SAMPLE][CHROM]):
                cnv_type = 'GAIN' if entry[2]>2 else 'DEL'
                print(CHROM, entry[0], entry[1], cnv_type, entry[3], entry[4], entry[5], entry[6])
                #GENE, TARGET_IND = entry[0], entry[1]

        


def del_fun(x,s1,s2,p):
    return abs(p*norm.pdf(x,1,s1) - (1-p)*norm.pdf(x,0.5,s2))

def gain_fun(x,s1,s2,p):
    return abs(p*norm.pdf(x,1,s1) - (1-p)*norm.pdf(x,1.5,s2))

def dg_del_fun(x,s,p,q):
    return abs((1-p-q)*norm.pdf(x,1,s) - p*norm.pdf(x,0.5,s) )

def dg_gain_fun(x,s,p,q):
    return abs((1-p-q)*norm.pdf(x,1,s) - q*norm.pdf(x,1.5,s) )

def del_gain_fun(x,s1,s2,s3,p,q):
    return ((1-p-q)*norm.pdf(x,1,s1) + p*norm.pdf(x,0.5,s2) + q*norm.pdf(x,1.5,s3))

#TODO
def plot_target(FNAME, MIN_IND, refSAMPLE):
    from matplotlib.patches import Rectangle

    fig, ax = plt.subplots(1,1)
    data = pd.read_csv(FNAME, sep='\t')
    SAMPLES = data.columns.values[4:]
    for SAMPLE in SAMPLES:
        plt.plot(data['pos'], data[SAMPLE], 'b')
    plt.plot(data['pos'], data[refSAMPLE], 'c')
    ax.add_patch(Rectangle( (data['pos'][MIN_IND]-0.5, -2),1, 4 , color='yellow',  linewidth=0.01))
    
    plt.title(FNAME)
    plt.xlabel('Genomic Position [bp]')
    plt.ylabel(r'$f_{smooth}$')
    plt.ylim([-2,2])
    plt.savefig(os.path.basename(FNAME).split('.')[0]+'.smooth.pdf')
    plt.close()

if __name__ == "__main__":
    plot_target(sys.argv[1])

