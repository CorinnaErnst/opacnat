import argparse
import numpy as np
import pandas as pd
import sys
import os
from scipy.stats import gmean
#from multiprocessing import Pool
#R
import rpy2
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
import rpy2.robjects as robjects
from rpy2.robjects.functions import SignatureTranslatedFunction


def analyse(INFILE, OUTFOLDER='gam_fitted', sample_cov_fraction = 0.66, target_cov_fraction=0.33, min_target_len=20, corr_thresh=0, ncontrols=30, MAX_TARGET_LEN = 300, OVERLAP_LEN = 50, nThreads=1):

    pd.options.mode.chained_assignment = None
    pandas2ri.activate()
    data = pd.read_csv(INFILE, sep='\t')
    SAMPLES = data.columns.values[4:]
    sys.stderr.write('...processing {} samples\n'.format(len(SAMPLES)))

    #delete samples with fraction of uncovered position below given threshold
    def delete_low_coverage_samples(d, cov_fraction=0.5):
        sys.stderr.write('### COVERAGE FILTER (sample-wise)\n')
        w, L = len(d.columns.values), len(d)
        del_col_inds = []
        for cind,SAMPLE in zip(range(4,w), d.columns.values[4:]):
            if len(d.loc[d[SAMPLE]==0])/L >= sample_cov_fraction: 
                sys.stderr.write('... deleting '+SAMPLE+' ( zero amount = '+str(len(d.loc[d[SAMPLE]==0])/L)+')\n')
                del_col_inds.append(SAMPLE)
        for colname in del_col_inds: d =d.drop(colname, 1)
        return d
    data = delete_low_coverage_samples(data,sample_cov_fraction)

    SAMPLES = data.columns.values[4:]
    sys.stderr.write('... {} sample(s) remained\n'.format(len(SAMPLES)))

    ## TARGET FILTER
    DTARGETS = data.groupby(['chr', 'gene','target_ind']).size()
    nTARGETS = len(DTARGETS)
    sys.stderr.write('### TARGET FILTER (' + str(nTARGETS) +' targets)\n')
    for i in range(nTARGETS):
        GENE, TIND = DTARGETS.index.values[i][1], DTARGETS.index.values[i][2]
        DTARGET = data.loc[(data['gene'] == GENE) & (data['target_ind'] == TIND)]
        L = len(DTARGET)
        if L < min_target_len:
            data = data.ix[~((data['gene']==GENE ) & (data['target_ind']==TIND) )]
            sys.stderr.write('... ' + GENE + ', target ' + str(TIND) + ' can not be evaluated: too short (' + str(L) + ' positions)\n')
        else:
            #TARGET COVERAGE FILTER
            del_inds = np.where(np.array(DTARGET[SAMPLES].sum()/len(DTARGET))<1)[0]
            #get targets to delete 
            if len(del_inds)/len(SAMPLES)>target_cov_fraction: 
                #remove target from data
                data = data.ix[~((data['gene']==GENE ) & (data['target_ind']==TIND) )]
                #sys.stderr.write('... ' + GENE+ ', target ' + str(TIND) + ' can not be evaluated due to low coverage ({})\n'.format(TARGETS.ix[i,SAMPLES]))
                sys.stderr.write('... ' + GENE+ ', target ' + str(TIND) + ' can not be evaluated due to low coverage ({})\n'.format(len(del_inds)/len(SAMPLES)))
            #XXX DEBUG
    #        elif len(del_inds)>0:
    #            print('###', GENE, TIND, len(del_inds), SAMPLES[del_inds])


    data.index = range(len(data)) 
    CHROMS = data['chr'].unique()

    DTARGETS =  data.groupby(['chr', 'gene','target_ind']).size()
    _chroms = [_[0] for _ in DTARGETS.index.values]
    _genes = [_[1] for _ in DTARGETS.index.values]
    _inds = [_[2] for _ in DTARGETS.index.values]
    #required for correlation
    TARGETS=pd.concat([pd.DataFrame(_chroms, columns=['chr']), pd.DataFrame(_genes, columns=['gene']), pd.DataFrame(_inds, columns=['target_ind']) ], axis=1)
    TARGETS = pd.concat([TARGETS, pd.DataFrame(np.ones( (len(TARGETS), len(SAMPLES)) ), columns=SAMPLES ) ], axis=1)
    nTARGETS = len(TARGETS)
    for i in range(nTARGETS):
        GENE, TIND = DTARGETS.index.values[i][1], DTARGETS.index.values[i][2]
        DTARGET = data.loc[(data['gene'] == GENE) & (data['target_ind'] == TIND)]
        TARGETS.ix[i,SAMPLES] = np.array(DTARGET[SAMPLES].sum(axis=0)/L)

    TCORR = TARGETS[SAMPLES].corr()
    CORR_MEANS = (TCORR.sum() -1)/(len(SAMPLES)-1)
    
    get_constant_cooks = robjects.r('''
    get_cooks <- function(y) {
        m <- lm(as.numeric(y)~1)
        return(cooks.distance(m))
    }
    ''')
    
    ROW_INDS = []
    sys.stderr.write("Sample-wise Mean Target Correlations: \n...Min={}, \n...Max={}, \n...Mean={}, \n...Median={}, \n...Std={}\n".format(np.min(CORR_MEANS), np.max(CORR_MEANS), np.mean(CORR_MEANS), np.median(CORR_MEANS), np.std(CORR_MEANS) ))
    cooks = np.array(get_constant_cooks(CORR_MEANS))

    if corr_thresh == 0:
        corr_thresh = np.inf
        for i in range(len(SAMPLES)):
            if cooks[i]>4/len(SAMPLES) and CORR_MEANS[i]<np.mean(CORR_MEANS):
                print(SAMPLES[i], CORR_MEANS[i], cooks[i])
                ROW_INDS.append(i)
            elif CORR_MEANS[i] < corr_thresh:
                #XXX define corr_thresh #TODO obligatory argument corr_thresh
                corr_thresh = CORR_MEANS[i]
    print('...corr_thresh={}'.format(corr_thresh))
    print('###')
#    for ind in np.where(CORR_MEANS<corr_thresh)[0]:
#        #print(SAMPLES[ind], CORR_MEANS[ind])
#        ROW_INDS.append(ind)
    
    
    # dataframe of correlation under exclusion of single chromosomes(colums) per sample (rows)
    DCORR = pd.DataFrame(np.zeros((len(SAMPLES),len(CHROMS))), index=SAMPLES, columns=CHROMS)
    for CHROM in CHROMS:
        tmpTARGETS = TARGETS[TARGETS['chr'] != CHROM]
        #print('chrom'+str(CHROM)+':', len(tmpTARGETS), 'targets') #DEBUG only
        tmpCORR = tmpTARGETS[SAMPLES].corr()
        tmpCORR_MEANS = tmpCORR.sum() -1
        DCORR[CHROM] = tmpCORR_MEANS/(len(SAMPLES)-1)
    
    # SAMPLES below thresholds (row) x CHROMS (col) : correlation values
    DCORR.iloc[ROW_INDS,:].to_csv('corrs.tsv', sep='\t', index=True)



    COOKS_CORRS = DCORR.iloc[ROW_INDS,:]
    DEL_INDS = []
    #iterate over samples
    for SAMPLE, ROW_IND in zip(SAMPLES[ROW_INDS], ROW_INDS):
        D = DCORR.iloc[ROW_IND,:]
        cooks = np.array(get_constant_cooks(D))
        cook_inds = np.where(cooks>4/len(D))[0]
        if len(cook_inds):
            #TODO: check if correlation > thresh
            CHROM_INDS = [_ for _ in cook_inds if D[DCORR.columns.values[_]] > corr_thresh]
            
        if len(cook_inds) and len(CHROM_INDS):
            print('...', SAMPLE, 'without chrom(s)', ', '.join([str(_) for _ in DCORR.columns.values[CHROM_INDS]]), '>', corr_thresh )
            #get chrom with highest correlation
            MAX_CORR, MAX_CHROM_IND = -np.inf, None
            for CHROM_IND in CHROM_INDS:
                CHROM = DCORR.columns.values[CHROM_IND]
                if D[CHROM] > MAX_CORR:
                    MAX_CORR, MAX_CHROM_IND = D[CHROM], CHROM_IND
                #XXX TODO get dictionary of outsourced chroms per sample and replace values in TCORR with max correlation
                #print('MAX', DCORR.columns.values[CHROM_IND], D[CHROM]) #DEBUG
            #...and add corresponding value to CORR_MEANS
                CORR_MEANS[SAMPLE] = D[CHROM]
                 
        else:
            #TODO report correlation
            sys.stderr.write('...' + SAMPLE + ' not evaluable due to low correlation ({})\n'.format(CORR_MEANS[ROW_IND]))
            DEL_INDS.append(ROW_IND)

    data =data.drop(SAMPLES[DEL_INDS], 1)
    CORR_MEANS = CORR_MEANS.drop(SAMPLES[DEL_INDS])
    sys.stderr.write('{}/{} samples removed\n'.format(len(DEL_INDS), len(SAMPLES)))
    TARGETS = TARGETS.drop(SAMPLES[DEL_INDS],1)

    #TODO check if really necessary
    SAMPLES = data.columns.values[4:]
    if len(SAMPLES) < ncontrols+1:
        sys.stderr.write('No. of samples with sufficient correlation to other samples < no. of required control samples. Consider to choose a correlation threshold < {}. ...TERMINATING\n'.format(corr_thresh) )
        sys.exit(0)
    
    #XXX
#    # region filter
#    sys.stderr.write('### REGION FILTER (position-wise)\n')
#    W, L = len(SAMPLES), len(data)
#    del_inds = np.where(data[SAMPLES].sum(axis=1)/W<pos_cov_fraction)[0]
#    data = data.drop(del_inds)
#    #reindex
#    data.index = range(1,len(data)+1) 
#    sys.stderr.write('... {} of {} positions deleted\n'.format(len(del_inds), L))
    CHROMS = data['chr'].unique()
    TCORR = TARGETS[SAMPLES].corr()


    ### NORMALIZATION
    sys.stderr.write('### INTER-SAMPLE COVERAGE NORMALIZATION\n')
#    def get_rfac(t):
#        d, divisors = t[0], t[1]
#        return np.median([d.iloc[_]/divisors[_] for _ in range(len(d))])
    def get_divisor(_a):
        _a = np.array(_a,dtype=float) #XXX dtype was int before
        _a = _a[np.where(_a>0)]
        return gmean(_a)#np.prod(_a**(1/len(_a)),dtype=float)
    DIVISORS = np.array(TARGETS.iloc[:,4:].apply(get_divisor, axis=1))
    args = [(TARGETS[_SAMPLE], DIVISORS) for _SAMPLE in SAMPLES]
    RFACS = pd.Series(np.array([get_rfac(arg) for arg in args]), index=SAMPLES)

    CORR_MEANS = CORR_MEANS.sort_values(ascending=False)
    
    
################
    #XXX this belongs into the function that will run in parallel
    
    
    sys.stderr.write('### GAM FITTING\n')
    mgcv = importr('mgcv') #TODO data.table is a required R package

    get_family = robjects.r('''
    get_family <- function(thetaa) {
        return(nb(theta=thetaa))
    }
    ''')
    
    
    robjects.r('''
    getVars <- function(formula) {
        chFormula <- as.character(formula)
        #remove whitespaces and split at plus signs
        variables <- gsub(" ", "", strsplit(chFormula[length(chFormula)],"\\\\+")[[1]])
        temp <- sapply(sapply(variables, function(m) regmatches(m,regexec("by=(.+)\\\\)",m))), function(y) y[2]) # returns c("x", "x")
        res <- gsub(",(.+)", "", temp) # res = c("x", "x")
        return(res)
    }
    
    ''')
    
    getFunctions = robjects.r('''
        getFunctions <- function(mod, data){
            pred <- mgcv::predict.gam(mod, type = "iterms", se.fit = TRUE)
            res <- data.table::data.table(matrix(0, length(unique(data$pos)), 4))
            nameslist <- c(colnames(pred$fit), paste("se", colnames(pred$se.fit), sep = "."))
            vars <- getVars(mod$formula)[-1]
            data.table::setnames(res, names(res), nameslist)
            # f_all
            intercept <- coefficients(mod)[1]
            for(ii in 1:2) {
                if(ii == 1) intercept <- coefficients(mod)[1]
                else intercept <- 0
                label <- mod$smooth[[ii]]$label
                selabel <- paste("se", label, sep = ".")
                if(is.na(vars[ii])) {
                    uniquepos <- !duplicated(data[, "pos"])
                    res[[label]] <- pred$fit[uniquepos, label] + intercept
                    res[[selabel]] <- pred$se.fit[uniquepos, label]
                }
                else {
                    uniquepos <- !duplicated(data[as.logical(data[[vars[ii]]]), "pos"])
                    res[[label]] <- pred$fit[as.logical(data[[vars[ii]]]),][uniquepos, label] + intercept
                    res[[selabel]] <- pred$se.fit[as.logical(data[[vars[ii]]]),][uniquepos, label]
                }
            }
            return(res)
    }
    ''')
#############

    #TODO calling on exclusively on specified targets or chromosomes
    for CHROM in CHROMS:
        DCHR = data.loc[data['chr'] == CHROM]
        GENES = DCHR['gene'].unique()
        for GENE in GENES:
            
            DGENE = DCHR.loc[DCHR['gene'] == GENE]
            TARGET_INDS = DGENE['target_ind'].unique()
            sys.stderr.write('{}: {} targets\n'.format(GENE, len(TARGET_INDS)))
            for tind in range(len(TARGET_INDS)):
            
                SAMPLES = data.columns.values[4:]
                # heuristic check if there might be a cnv
                DTARGET = DGENE.loc[DGENE['target_ind']==TARGET_INDS[tind]]
                L = len(DTARGET)
                VALS = DTARGET.sum()[SAMPLES]
                #print(VALS/RFACS)
                M = np.mean(VALS/RFACS)
                HeurFC = (VALS/RFACS)/M
                if ((0.75 < HeurFC) & (HeurFC<1.25)).sum() == len(SAMPLES):
                    sys.stderr.write('{} {} OK\n'.format(GENE, TARGET_INDS[tind]))
                
                else:

                    FAILURE_INDS = set(range(len(SAMPLES))).difference(set(np.where(  (0.75 < HeurFC) & (HeurFC<1.25))[0] )) 
                    FAILURE_SAMPLES = SAMPLES[list(FAILURE_INDS)]


                    for SAMPLE in FAILURE_SAMPLES:
                        DTARGET = DGENE.loc[DGENE['target_ind']==TARGET_INDS[tind]]
                        #SAMPLES = data.columns.values[4:]
                        #get control samples
                        SAMPLE_SET = TCORR[SAMPLE].sort_values(ascending=False).index[:ncontrols+1] #+1 to include sample of interest (corr=1)
                        
                        #XXX TODO
                        #SAMPLES = SAMPLE_SET

                        DTARGET = pd.concat([DTARGET[DTARGET.columns.values[:4]], DTARGET[SAMPLE_SET]], axis=1)

                        k = max(4, min(10, int(L/20)))
                        if L > MAX_TARGET_LEN: 
                            # T-O-T-O-T
                            nTARGETS = int((L-OVERLAP_LEN)/(MAX_TARGET_LEN-OVERLAP_LEN)) +1 if (L-OVERLAP_LEN)/(MAX_TARGET_LEN-OVERLAP_LEN) % 1 !=0 else int((L-OVERLAP_LEN)/(MAX_TARGET_LEN-OVERLAP_LEN))

                            subL = int((L-OVERLAP_LEN)/nTARGETS)
                            SUBTARGETS = [(0,subL+OVERLAP_LEN)]
                            for i in range(nTARGETS-1):
                                SUBTARGETS.append( (SUBTARGETS[-1][1]-OVERLAP_LEN, SUBTARGETS[-1][1]+subL) )
                            
                        else: SUBTARGETS = [(0,L)]
                        sys.stderr.write('{} {} : {}  ( {}bp, {} subtarget(s) )\n'.format(GENE, TARGET_INDS[tind], SAMPLE, L, len(SUBTARGETS) ))
                        
                        #prepare output
                        #TODO
                        OUT_fsample = pd.concat([DTARGET[DTARGET.columns.values[:4]],pd.DataFrame(index=DTARGET.index, columns=SAMPLE_SET)],axis=1)
                        OUT_fsample_se = pd.concat([DTARGET[DTARGET.columns.values[:4]],pd.DataFrame(index=DTARGET.index, columns=SAMPLE_SET)],axis=1)
                        OUT_fall = pd.concat([DTARGET[DTARGET.columns.values[:4]],pd.DataFrame(index=DTARGET.index, columns=SAMPLE_SET)],axis=1)
                        OUT_fall_se = pd.concat([DTARGET[DTARGET.columns.values[:4]],pd.DataFrame(index=DTARGET.index, columns=SAMPLE_SET)],axis=1)
                        
                        #iterate over subtargets
                        for subtarget in SUBTARGETS:
                            st_start, st_stop = subtarget[0], subtarget[1]
                            dstart = st_start+int(OVERLAP_LEN/2) if subtarget != SUBTARGETS[0] else st_start 
                            dstop = st_stop-int(OVERLAP_LEN/2) if subtarget != SUBTARGETS[-1] else st_stop
                            
                            
                            #TODO
                            d = DTARGET.iloc[st_start:st_stop,:]
                            subL = len(d)
                            DR=np.zeros((subL*len(SAMPLE_SET),), dtype=[("pos", int ), ("value", int), ("offset", float), ('sample', int) ])
                            DR['pos'] = len(SAMPLE_SET) * list(d['pos'])
                            for j in range(len(SAMPLE_SET)):
                                DR['value'][j*subL:j*subL+subL] = d.iloc[:,4+j]
                                ###XXX
                                DR['offset'][j*subL:j*subL+subL] = np.log(RFACS[SAMPLE_SET[j]])
                            
                            
                            #RESULTS = []
                            THETAS = []
                            
                            
                            DR = pd.DataFrame(DR)
                            
                            for j in range(len(SAMPLE_SET)):
                                try: 
                                    #_DR = DR.copy()
                                    DR['sample'].iloc[j*subL:j*subL+subL] = 1

                                    r_formula = robjects.Formula('value ~ offset(offset) + s(pos, bs = \"ps\", k={}, m = 2) + s(pos, by = sample, bs = \"ps\", k={}, m = 2)'.format(k,k))
                                    family = robjects.r['nb']
                                    mod = mgcv.gam(r_formula, family, DR, lamda=[100,100])
                                    
                                    
                                    #XXX DEBUG
                                    #theta = str(mod[3][0][0]).split('(')[1][:-1].replace(',', '.')
                                    #THETAS.append(float(theta) )
                                    
                                    result = getFunctions(mod, DR) #s(pos) s(pos):sample  se.s(pos) se.s(pos):sample

                                    #TODO compute once
                                    #start and stop ind in result to copy 
                                    rstart = int(OVERLAP_LEN/2) if subtarget != SUBTARGETS[0] else 0 
                                    rstop = len(np.array(result[0]))-int(OVERLAP_LEN/2) if subtarget != SUBTARGETS[-1] else len(np.array(result[0]))

                                    OUT_fall[SAMPLE_SET[j]][dstart:dstop] = np.array(result[0])[rstart:rstop]
                                    OUT_fsample[SAMPLE_SET[j]][dstart:dstop] = np.array(result[1])[rstart:rstop]
                                    OUT_fall_se[SAMPLE_SET[j]][dstart:dstop] = np.array(result[2])[rstart:rstop]
                                    OUT_fsample_se[SAMPLE_SET[j]][dstart:dstop] = np.array(result[3])[rstart:rstop]
                                    
                                    #confidence interval: c(y  - 1.96*se.y, y  + 1.96*se.y)
                                    DR['sample'].iloc[j*subL:j*subL+subL] = 0

                            

                                except:
                                    sys.stderr.write('...SKIPPING '+SAMPLE_SET[j]+'\n')
                                    OUT_fsample[SAMPLE_SET[j]][dstart:dstop] = None
                                    OUT_fsample_se[SAMPLE_SET[j]][dstart:dstop] = None
                                    OUT_fall[SAMPLE_SET[j]][dstart:dstop] = None
                                    OUT_fall_se[SAMPLE_SET[j]][dstart:dstop] = None
                                    
                                    continue
                                sys.stderr.write('.')
                            sys.stderr.write('\n')

                        #write to files
                        #sys.stderr.write('...writing to files.\n')
                        outfile1 = os.path.join(OUTFOLDER, '{}_{}_{}.tsv'.format(GENE, TARGET_INDS[tind], SAMPLE))
                        outfile2 = os.path.join(OUTFOLDER, '{}_{}_{}_all.tsv'.format(GENE, TARGET_INDS[tind], SAMPLE))
                        outfile3 = os.path.join(OUTFOLDER, '{}_{}_{}_se.tsv'.format(GENE, TARGET_INDS[tind], SAMPLE))
                        outfile4 = os.path.join(OUTFOLDER, '{}_{}_{}_fall_se.tsv'.format(GENE, TARGET_INDS[tind], SAMPLE))
                        OUT_fsample.to_csv(outfile1, sep='\t', index=False)
                        OUT_fall.to_csv(outfile2, sep='\t', index=False)
                        OUT_fsample_se.to_csv(outfile3, sep='\t', index=False)
                        OUT_fall_se.to_csv(outfile4, sep='\t', index=False)
                    

def get_rfac(t):
    d, divisors = t[0], t[1]
    return np.median([d.iloc[_]/divisors[_] for _ in range(len(d))])


#TODO not used at the moment (improved version required for parallel gam fitting)
def process_one_target(t):

    
    d, tind, samples, RFACS = t

    sys.stderr.write('...'+str(tind)+ ' (' + str(len(d)) +' positions)\n')
    mgcv = importr('mgcv')
    robjects.r('''
    getVars <- function(formula) {
        chFormula <- as.character(formula)
        #remove whitespaces and split at plus signs
        variables <- gsub(" ", "", strsplit(chFormula[length(chFormula)],"\\\\+")[[1]])
        temp <- sapply(sapply(variables, function(m) regmatches(m,regexec("by=(.+)\\\\)",m))), function(y) y[2]) # returns c("x", "x")
        res <- gsub(",(.+)", "", temp) # res = c("x", "x")
        return(res)
    }
    
    ''')
    
    getFunctions = robjects.r('''
        getFunctions <- function(mod, data){
            pred <- mgcv::predict.gam(mod, type = "iterms", se.fit = TRUE)
            res <- data.table::data.table(matrix(0, length(unique(data$pos)), 4))
            nameslist <- c(colnames(pred$fit), paste("se", colnames(pred$se.fit), sep = "."))
            vars <- getVars(mod$formula)[-1]
            data.table::setnames(res, names(res), nameslist)
            # f_all
            intercept <- coefficients(mod)[1]
            for(ii in 1:2) {
                if(ii == 1) intercept <- coefficients(mod)[1]
                else intercept <- 0
                label <- mod$smooth[[ii]]$label
                selabel <- paste("se", label, sep = ".")
                if(is.na(vars[ii])) {
                    uniquepos <- !duplicated(data[, "pos"])
                    res[[label]] <- pred$fit[uniquepos, label] + intercept
                    res[[selabel]] <- pred$se.fit[uniquepos, label]
                }
                else {
                    uniquepos <- !duplicated(data[as.logical(data[[vars[ii]]]), "pos"])
                    res[[label]] <- pred$fit[as.logical(data[[vars[ii]]]),][uniquepos, label] + intercept
                    res[[selabel]] <- pred$se.fit[as.logical(data[[vars[ii]]]),][uniquepos, label]
                }
            }
            return(res)
    }
    ''')
    L = len(d)
    k = max(4, min(10, int(L/20)))
    
    DR=np.zeros((len(d)*len(samples),), dtype=[("pos", int ), ("value", int), ("offset", float), ('sample', int) ])
    DR['pos'] = len(samples) * list(d['pos'])
    #DR['strand'] = '*'
    for i in range(len(samples)):
        DR['value'][i*L:i*L+L] = d.iloc[:,4+i]
        DR['offset'][i*L:i*L+L] = np.log(RFACS[i])
    
    
    RESULTS = []
    for i in range(len(samples)):
        #sys.stderr.write('...'+samples[i]+'\n')
        try: 
            _DR = DR.copy()
            _DR['sample'][i*L:i*L+L] = 1

            r_formula = robjects.Formula('value ~ offset(offset) + s(pos, bs = \"ps\", k={}, m = 2) + s(pos, by = sample, bs = \"ps\", k={}, m = 2)'.format(k,k))
            family = robjects.r['nb']

            _DR = pd.DataFrame(_DR)
            mod = mgcv.gam(r_formula, family, _DR, lamda=[100,100])

            result = getFunctions(mod, _DR)
            RESULTS.append(result)

        except:
            sys.stderr.write('...SKIPPING '+samples[i]+'\n')
            RESULTS.append(None)
            continue
            
    return RESULTS


