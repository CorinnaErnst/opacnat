import os
import sys

from opacnat.analyse import analyse
from opacnat.parse_bam import parse_bams
from opacnat.caller import call

def parse_bam(panelfile, BAMS, threads, out_file):
    parse_bams(panelfile, BAMS, threads, out_file)
    
def gam(infile, outfolder, sample_cov_frac, target_cov_frac, min_target_len, corr_thresh, ncontrols, MAX_TARGET_LEN, OVERLAP_LEN):
    try:
        os.stat(outfolder)
    except:
        os.mkdir(outfolder)
    if not 0 < sample_cov_frac < 1:
        sys.exit('...min-sample-cov-frac has to be between 0 and 1 (you chose {}) ...TERMINATING'.format(sample_cov_frac))
    if not 0 < target_cov_frac < 1:
        sys.exit('...max-target-cov-frac has to be between 0 and 1 (you chose {}) ...TERMINATING'.format(target_cov_frac))
    if not min_target_len > 0:
        sys.exit('...minimum target length has to be >0 (you chose {}) ...TERMINATING'.format(min_target_length))
    if not 0 <= corr_thresh < 1:
        sys.exit('...corr-thresh (minimum of mean correlation to other samples) has to be between 0 and 1 (you chose {}) ...TERMINATING'.format(corr_thresh))
    analyse(infile, outfolder, sample_cov_frac, target_cov_frac, min_target_len, corr_thresh, ncontrols, MAX_TARGET_LEN, OVERLAP_LEN, nThreads=1) 
    
def call_cnvs(infolder, plotFlag,targetFlag, prob_thresh, prior):
    if not 0 < prob_thresh < 1:
        sys.exit('...conditional probability threshold has to be between 0 and 1 (you chose {}) ...TERMINATING'.format(prob_thresh))
    if not 0 < prior < 1:
        sys.exit('...prior probability of CNV events has to be between 0 and 1 (you chose {}) ...TERMINATING'.format(prior))
    call(infolder, plotFlag,targetFlag, prob_thresh, prior)
