import argparse
import numpy as np
import pandas as pd
import os
from multiprocessing import Pool
from collections import defaultdict
import sys
import pysam

#TODO one parse_bam file for both forward and reverse

def parse_bams(PANEL_FILE, BAMS, threads, out_file):
    REGIONS = dict() #REGIONS[_gene][_start] = _stop # for each region!
    REGION_CHROMS = dict() #REGION_CHROMS[_gene] = (_chr, _start) #one start per gene!!!

    pos_sum = 0
    CHROM_TO_GENES = dict() #per chrom sorted list of genes
    GENE_TO_STARTS = dict() #per gene: sorted starts on GENE_TO_CHROM[gene]
    START_TO_STOP = dict() # FOREACH target (on chrom): start --> stop


    with open(PANEL_FILE) as infile:
        for line in infile:
            ll = line.rstrip().split('\t')
            chrom, start, stop = ll[:3]
            
            start, stop = int(start), int(stop)
            pos_sum += stop-start+1
            gene = 'unknown' if len(ll) < 4 else ll[3]
            if chrom not in CHROM_TO_GENES: CHROM_TO_GENES[chrom] = [gene]
            elif gene not in CHROM_TO_GENES[chrom]:  CHROM_TO_GENES[chrom].append(gene)
            if gene not in GENE_TO_STARTS: GENE_TO_STARTS[gene] = [start]
            else: GENE_TO_STARTS[gene].append(start)
            if chrom not in START_TO_STOP: START_TO_STOP[chrom] = dict()
            START_TO_STOP[chrom][start] = stop
            

    _ind=0
    d=np.zeros((pos_sum,), dtype=[("gene",np.str_,30),("target_ind", 'i4' ), ("chr", np.str_,2), ("pos", 'i4') ])
    POS_TO_POS_IND = dict()
    for chrom in sorted(CHROM_TO_GENES.keys()):
        POS_TO_POS_IND[chrom] = dict()
        for gene in CHROM_TO_GENES[chrom]:
            target_ind = 0
            for start in GENE_TO_STARTS[gene]:
                target_ind +=1
                stop = START_TO_STOP[chrom][start]
                for pos in range(start, stop+1):
                    #TODO 
                    d[_ind,] = (gene, target_ind, chrom, pos)
                    POS_TO_POS_IND[chrom][pos] = _ind
                    _ind+=1
    df=pd.DataFrame(d)

    #PARSING
    pool = Pool(processes=threads)
    args = [(FILE, pos_sum, POS_TO_POS_IND, START_TO_STOP) for FILE in BAMS]
    results = pool.map(parse_bam, args)
    for j in range(len(results)):
        label = '.'.join(os.path.basename(BAMS[j]).split('.')[:-1])
        #add to data frame
        df[label] = results[j]

    df.to_csv(out_file, sep='\t', index=False)


def parse_bam(t):
    fname, pos_sum, d, START_TO_STOP = t[0], t[1], t[2], t[3] #d=POS_TO_POS_IND[c][p]
    CHROMS = d.keys()
    try:
        sys.stderr.write('...processing {}\n'.format(fname))
        samfile = pysam.AlignmentFile(fname, "rb")
        ###
        # .bam file (e.g., SeqPilot download) might not contain all references given in panel specification
        BAM_REFS = [_['SN'] for _ in samfile.header['SQ']]
        #print('sam_refs', BAM_REFS)
        ###
        _d = np.zeros(pos_sum, dtype=np.int)
        for CHROM in CHROMS:
            if CHROM in BAM_REFS:
                for start in START_TO_STOP[CHROM]:
                    for r in samfile.fetch(CHROM, start-250,START_TO_STOP[CHROM][start]+250):
                        if not r.is_duplicate:
                            _pos = round(r.reference_start + r.query_length/2)
                            if _pos in d[CHROM]:
                                _d[d[CHROM][_pos]] = _d[d[CHROM][_pos]]+1
            else: sys.stderr.write('... reference {} is not specified in .bam file {} ...SKIPPING\n'.format(CHROM, fname))
        return _d
    except IOError: print('...unable to open {} ...SKIPPING'.format(fname))

if __name__ == "__main__":
    p = argparse.ArgumentParser(description='''OPaCNAT (On Panel Copy Number Analysis Tool) --- CNV Detection on multi gene panels''')
    subparsers = p.add_subparsers(dest='subcommand')
    parse = subparsers.add_parser("parse", help="parse .bam files and create a data frame of on-target read counts")
    parse.add_argument("--infile", "-i", metavar="bedfile", nargs=1, help = ".bed file specifying panel targets (NOTE: due to the .bed format specification, positions are assumed to be reported as 0-based)", required=True)
    parse.add_argument("--output", "-o", metavar="outfile", help = "output file (tab-seprated), default=readcounts.tsv", default='readcounts.tsv', type=str)
    parse.add_argument("--threads", "-t", metavar='INT', help="number of parallel threads (default=1)", default=1, type=int)
    parse.add_argument("BAMS", nargs ='+', help="List of input .bam files.")
    args = p.parse_args()
    
    if args.subcommand == "parse":
        parse_bams(args.infile[0],
        args.BAMS,
        args.threads,
        args.output
        )


