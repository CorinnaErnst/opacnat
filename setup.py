# coding: utf-8
from setuptools import setup
import sys

if sys.version_info < (3,2):
	sys.stdout.write("At least Python 3.2 is required.\n")
	sys.exit(1)

__version__ = '0.4.2'

setup(
	name = 'opacnat',
	version = __version__,
	author = 'Corinna Ernst',
	author_email = 'corinna.ernst@uk-koeln.de',
	description = 'On Panel Copy Number Analysis Tool -- CNV Detection on Multi Gene Panels',
	license = 'MIT',
	url = 'https://bitbucket.org/CorinnaErnst/opacnat',
	packages = ['opacnat'],
	scripts = ['bin/opacnat'],
	install_requires=["numpy", "scipy", "pandas", "pysam", "rpy2","matplotlib"],
	test_suite = 'nose.collector',
	classifiers = [
		"Development Status :: 3 - Alpha",
		"Environment :: Console",
		"Intended Audience :: Science/Research",
		"License :: OSI Approved :: MIT License",
		"Natural Language :: English",
		"Programming Language :: Python :: 3",
		"Topic :: Scientific/Engineering :: Bio-Informatics"
	]
)
